# hackathon-cicd-demo

This repository contains a simple sample app. Actually it is a static file (index.html), served by a web server (nginx).

The web server and index.html are packaged together using the Dockerfile.

In the .gitlab-ci.yml you can see how the docker file can be build and pushed to GitLab.com's registry.

When someone pushes to master, uploaded containers are tagged with "staging" and if someone pushes a tag, that container build will be tagged with "production".

In the deploy stage, the CI/CD pipeline of https://gitlab.com/stefreak/hackathon-cicd-infrastructure/pipelines will be triggered, which will eventually create virtual machines in OpenStack running the docker containers that were uploaded to the registry.

See the other README for more information on that: https://gitlab.com/stefreak/hackathon-cicd-infrastructure

# What you could do
- Write & Build docker files manually
- Replicate the CI/CD pipleine in your own Project in GitLab
- Write a docker file for an application you wrote or you like :)
- 
