stages:
  - build
  - test
  - publish
  - deploy
  - cleanup

.docker-in-docker: &dind-template
  image: docker:stable
  variables:
    # When using dind service we need to instruct docker, to talk with the
    # daemon started inside of the service. The daemon is available with
    # a network connection instead of the default /var/run/docker.sock socket.
    #
    # The 'docker' hostname is the alias of the service container as described at
    # https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#accessing-the-services
    #
    # Note that if you're using Kubernetes executor, the variable should be set to
    # tcp://localhost:2375 because of how Kubernetes executor connects services
    # to the job container
    DOCKER_HOST: tcp://docker:2375/
    # When using dind, it's wise to use the overlayfs driver for
    # improved performance.
    DOCKER_DRIVER: overlay2
  services:
    - docker:dind
  before_script:
    - docker info
    - docker login registry.gitlab.com -u ${REGISTRY_USERNAME} -p ${REGISTRY_TOKEN}

build_image:
  stage: build
  <<: *dind-template
  script:
    - sed -i -e "s/COMMIT_HASH/${CI_COMMIT_SHORT_SHA} (${CI_COMMIT_REF_SLUG})/g" index.html
    - docker build -t registry.gitlab.com/stefreak/hackathon-cicd-demo:cicd-${CI_COMMIT_SHORT_SHA} .
    - docker push registry.gitlab.com/stefreak/hackathon-cicd-demo:cicd-${CI_COMMIT_SHORT_SHA}

test_image:
  stage: test
  image: alpine
  services:
    - name: registry.gitlab.com/stefreak/hackathon-cicd-demo:cicd-${CI_COMMIT_SHORT_SHA}
      alias: demo
  script:
    - apk add httpie
    - http --ignore-stdin http://demo --pretty all

publish_staging:
  <<: *dind-template
  stage: publish
  script:
    - docker pull registry.gitlab.com/stefreak/hackathon-cicd-demo:cicd-${CI_COMMIT_SHORT_SHA} || docker build -t registry.gitlab.com/stefreak/hackathon-cicd-demo:cicd-${CI_COMMIT_SHORT_SHA} .
    - docker tag registry.gitlab.com/stefreak/hackathon-cicd-demo:cicd-${CI_COMMIT_SHORT_SHA} registry.gitlab.com/stefreak/hackathon-cicd-demo:staging
    - docker push registry.gitlab.com/stefreak/hackathon-cicd-demo:staging
  environment:
    name: staging
  only:
    - master
    - tags

publish_production:
  <<: *dind-template
  stage: publish
  script:
    - docker pull registry.gitlab.com/stefreak/hackathon-cicd-demo:cicd-${CI_COMMIT_SHORT_SHA} || docker build -t registry.gitlab.com/stefreak/hackathon-cicd-demo:cicd-${CI_COMMIT_SHORT_SHA} .
    - docker tag registry.gitlab.com/stefreak/hackathon-cicd-demo:cicd-${CI_COMMIT_SHORT_SHA} registry.gitlab.com/stefreak/hackathon-cicd-demo:production
    - docker push registry.gitlab.com/stefreak/hackathon-cicd-demo:production
  environment:
    name: production
  only:
    - tags

deploy_immutable_infrastructure:
  stage: deploy
  image: alpine
  script:
    - apk add httpie
    # Token can be created here: https://gitlab.com/stefreak/hackathon-cicd-infrastructure/settings/ci_cd
    - http --ignore-stdin POST https://gitlab.com/api/v4/projects/10968265/trigger/pipeline token=6f5e279d736e438b011c9987d26c55 ref=master --pretty all
  only:
    - tags
    - master

cleanup_registry_cicd_tag:
  stage: cleanup
  image: alpine
  script:
    - apk add httpie
    # https://docs.gitlab.com/ee/api/container_registry.html#delete-repository-tags-in-bulk
    - 'http --ignore-stdin DELETE "https://gitlab.com/api/v4/projects/stefreak%2Fhackathon-cicd-demo/registry/repositories/430712/tags" PRIVATE-TOKEN:${REGISTRY_TOKEN} "name_regex=^cicd-${CI_COMMIT_SHORT_SHA}$" --pretty all'
  when: always
